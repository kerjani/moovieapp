package com.kerjani.moovieapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.kerjani.moovieapp.util.MovieItemUtils;
import com.squareup.picasso.Picasso;

import java.io.Serializable;

public class DetailsActivity extends AppCompatActivity {

	public static final String EXTRA_KEY_DATA = "DetailsActivity:EXTRA_KEY_DATA";
	public static final String BUNDLE_KEY_ITEM = "DetailsActivity:BUNDLE_KEY_ITEM";
	public static final String BUNDLE_KEY_TITLE = "DetailsActivity:BUNDLE_KEY_TITLE";
	public static final String BUNDLE_KEY_IMAGE_URL = "DetailsActivity:BUNDLE_KEY_IMAGE_URL";
	public static final String BUNDLE_KEY_DETAIL_FRAGMENT_CLASS_NAME = "DetailsActivity:BUNDLE_KEY_DETAIL_FRAGMENT_CLASS_NAME";

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);
		final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		final Bundle extras = getIntent() != null ? getIntent().getExtras() : null;
		if (extras != null && extras.containsKey(EXTRA_KEY_DATA)) {
			final Bundle data = extras.getBundle(EXTRA_KEY_DATA);
			final Serializable item = data.getSerializable(BUNDLE_KEY_ITEM);
			final String title = data.getString(BUNDLE_KEY_TITLE);
			final String imageUrl = data.getString(BUNDLE_KEY_IMAGE_URL);
			toolbar.setTitle(title);
			final ImageView toolbarImage = (ImageView) findViewById(R.id.toolbar_image);
			Picasso.with(this)
					.load(MovieItemUtils.getDetailmageUrl(imageUrl))
					.into(toolbarImage);
			final String detailsFragmentClassName = data.getString(BUNDLE_KEY_DETAIL_FRAGMENT_CLASS_NAME);
			loadDetailFragment(detailsFragmentClassName, item);
		}
		setSupportActionBar(toolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

	private void loadDetailFragment(final String detailFragmentClassName, final Serializable dataItem) {
		final Fragment detailFragment = Fragment.instantiate(this, detailFragmentClassName);
		final Bundle detailArgument = new Bundle();
		detailArgument.putSerializable(BaseDetailsFragment.BUNDLE_KEY_DETAIL_DATA, dataItem);
		detailFragment.setArguments(detailArgument);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.detail_content, detailFragment)
				.commit();
	}
}
