package com.kerjani.moovieapp.tv;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.kerjani.moovieapp.BaseDetailsFragment;
import com.kerjani.moovieapp.util.MovieItemUtils;
import com.omertron.themoviedbapi.model.tv.TVBasic;

/**
 *
 */
public class TvShowDetailsFragment extends BaseDetailsFragment<TVBasic> {
	@Override
	public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		titleTextView.setText(data.getName());
		ratingTextView.setText(MovieItemUtils.formatRating(data.getVoteAverage()));
		yearTextView.setText(MovieItemUtils.formatReleaseYear(data.getFirstAirDate()));
		genreTextView.setText(MovieItemUtils.formatGenres(data.getGenreIds()));
		overviewTextView.setText(data.getOverview());
	}
}
