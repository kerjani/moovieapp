package com.kerjani.moovieapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 *
 */
public abstract class BaseDetailsFragment<T> extends Fragment {
	public static final String BUNDLE_KEY_DETAIL_DATA = "DetailsActivity:BUNDLE_KEY_DETAIL_DATA";
	protected T data;

	protected AppCompatTextView titleTextView;
	protected AppCompatTextView ratingTextView;
	protected AppCompatTextView genreTextView;
	protected AppCompatTextView yearTextView;
	protected AppCompatTextView overviewTextView;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final Bundle args = getArguments();
		if (args != null && args.containsKey(BUNDLE_KEY_DETAIL_DATA)) {
			data = (T) args.getSerializable(BUNDLE_KEY_DETAIL_DATA);
		}
	}

	@Nullable
	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.fragment_movie_details, container, false);
		titleTextView = (AppCompatTextView) view.findViewById(R.id.movie_detail_txt_title);
		ratingTextView = (AppCompatTextView) view.findViewById(R.id.movie_detail_txt_rating);
		genreTextView = (AppCompatTextView) view.findViewById(R.id.movie_detail_txt_genres);
		yearTextView = (AppCompatTextView) view.findViewById(R.id.movie_detail_txt_year);
		overviewTextView = (AppCompatTextView) view.findViewById(R.id.movie_detail_txt_overview);
		return view;
	}
}
