package com.kerjani.moovieapp.person;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kerjani.moovieapp.R;
import com.kerjani.moovieapp.util.MovieItemUtils;
import com.omertron.themoviedbapi.model.person.PersonFind;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

/**
 *
 */
public class PersonsRecyclerViewAdapter extends RecyclerView.Adapter<PersonsRecyclerViewAdapter.ViewHolder> {
	private List<PersonFind> items;

	public PersonsRecyclerViewAdapter(List<PersonFind> items) {
		this.items = items;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.item_person, parent, false);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, int position) {
		final PersonFind data = items.get(position);
		holder.item = data;

		final Context context = holder.nameTextView.getContext();
		Picasso.with(context)
				.load(MovieItemUtils.getSmallImageUrl(data.getProfilePath()))
				.placeholder(R.drawable.placeholder_image)
				.into(holder.posterImageView);
		holder.nameTextView.setText(data.getName());
	}

	@Override
	public int getItemCount() {
		return items.size();
	}


	protected class ViewHolder extends RecyclerView.ViewHolder {
		public final AppCompatImageView posterImageView;
		public final AppCompatTextView nameTextView;
		public Serializable item;

		public ViewHolder(View view) {
			super(view);
			posterImageView = (AppCompatImageView) view.findViewById(R.id.item_person_img_poster);
			nameTextView = (AppCompatTextView) view.findViewById(R.id.item_person_txt_name);
		}

	}

}
