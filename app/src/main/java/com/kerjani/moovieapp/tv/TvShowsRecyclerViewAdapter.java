package com.kerjani.moovieapp.tv;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kerjani.moovieapp.DetailsActivity;
import com.kerjani.moovieapp.R;
import com.kerjani.moovieapp.communication.MooviesApi;
import com.kerjani.moovieapp.movie.MoviesRecyclerViewAdapter;
import com.kerjani.moovieapp.util.MovieItemUtils;
import com.omertron.themoviedbapi.model.tv.TVBasic;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 *
 */
public class TvShowsRecyclerViewAdapter extends RecyclerView.Adapter<MoviesRecyclerViewAdapter.ViewHolder> {
	private List<TVBasic> items;

	public TvShowsRecyclerViewAdapter(List<TVBasic> items) {
		this.items = items;
	}

	@Override
	public MoviesRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.item_movie, parent, false);
		return new MoviesRecyclerViewAdapter.ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(final MoviesRecyclerViewAdapter.ViewHolder holder, int position) {
		final TVBasic data = items.get(position);
		holder.item = data;

		final Context context = holder.parentView.getContext();
		Picasso.with(context)
				.load(MovieItemUtils.getSmallImageUrl(data.getPosterPath()))
				.placeholder(R.drawable.placeholder_image)
				.into(holder.posterImageView);

		holder.titleTextView.setText(data.getName());
		holder.ratingTextView.setText(MovieItemUtils.formatRating(data.getVoteAverage()));
		holder.yearTextView.setText(MovieItemUtils.formatReleaseYear(data.getFirstAirDate()));
		holder.genreTextView.setText(MovieItemUtils.formatGenres(data.getGenreIds()));
		holder.overviewTextView.setText(data.getOverview());

		holder.parentView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent detailsIntent = new Intent(context, DetailsActivity.class);

				detailsIntent.putExtra(DetailsActivity.EXTRA_KEY_DATA, MooviesApi
						.buildDetailsExtra(TvShowDetailsFragment.class.getName(), data, data.getName(),
								data.getBackdropPath()));
				context.startActivity(detailsIntent);

			}
		});
	}

	@Override
	public int getItemCount() {
		return items.size();
	}

}
