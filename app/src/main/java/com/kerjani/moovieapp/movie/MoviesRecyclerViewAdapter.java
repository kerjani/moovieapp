package com.kerjani.moovieapp.movie;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kerjani.moovieapp.DetailsActivity;
import com.kerjani.moovieapp.R;
import com.kerjani.moovieapp.communication.MooviesApi;
import com.kerjani.moovieapp.util.MovieItemUtils;
import com.omertron.themoviedbapi.model.movie.MovieBasic;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MovieBasic}
 */
public class MoviesRecyclerViewAdapter extends RecyclerView.Adapter<MoviesRecyclerViewAdapter.ViewHolder> {
	private List<MovieBasic> items;

	public MoviesRecyclerViewAdapter(List<MovieBasic> items) {
		this.items = items;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		final View view = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.item_movie, parent, false);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, int position) {
		final MovieBasic data = items.get(position);
		holder.item = data;

		final Context context = holder.parentView.getContext();
		Picasso.with(context)
				.load(MovieItemUtils.getSmallImageUrl(data.getPosterPath()))
				.placeholder(R.drawable.placeholder_image)
				.into(holder.posterImageView);

		holder.titleTextView.setText(data.getTitle());
		holder.ratingTextView.setText(MovieItemUtils.formatRating(data.getVoteAverage()));
		holder.yearTextView.setText(MovieItemUtils.formatReleaseYear(data.getReleaseDate()));
		holder.genreTextView.setText(MovieItemUtils.formatGenres(data.getGenreIds()));
		holder.overviewTextView.setText(data.getOverview());

		holder.parentView.setOnClickListener(v -> {
			Intent detailsIntent = new Intent(context, DetailsActivity.class);
			detailsIntent.putExtra(DetailsActivity.EXTRA_KEY_DATA, MooviesApi
					.buildDetailsExtra(MovieDetailsFragment.class.getName(), data, data.getTitle(),
							data.getBackdropPath()));
			context.startActivity(detailsIntent);

		});
	}

	@Override
	public int getItemCount() {
		return items.size();
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		public final View parentView;
		public final AppCompatImageView posterImageView;
		public final AppCompatTextView titleTextView;
		public final AppCompatTextView ratingTextView;
		public final AppCompatTextView genreTextView;
		public final AppCompatTextView yearTextView;
		public final AppCompatTextView overviewTextView;
		public Serializable item;

		public ViewHolder(View view) {
			super(view);
			parentView = view.findViewById(R.id.item_movie_content);
			view.setOnTouchListener((v, event) -> {
				// forward touch to the
				return parentView.dispatchTouchEvent(event);
			});
			posterImageView = (AppCompatImageView) view.findViewById(R.id.item_movie_img_poster);
			titleTextView = (AppCompatTextView) view.findViewById(R.id.item_movie_txt_title);
			ratingTextView = (AppCompatTextView) view.findViewById(R.id.item_movie_txt_rating);
			genreTextView = (AppCompatTextView) view.findViewById(R.id.item_movie_txt_genres);
			yearTextView = (AppCompatTextView) view.findViewById(R.id.item_movie_txt_year);
			overviewTextView = (AppCompatTextView) view.findViewById(R.id.item_movie_txt_overview);
		}

	}
}
