package com.kerjani.moovieapp.movie;

import android.os.Bundle;
import android.view.View;

import com.kerjani.moovieapp.BaseDetailsFragment;
import com.kerjani.moovieapp.util.MovieItemUtils;
import com.omertron.themoviedbapi.model.movie.MovieBasic;

/**
 */
public class MovieDetailsFragment extends BaseDetailsFragment<MovieBasic> {

	@Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		titleTextView.setText(data.getTitle());
		ratingTextView.setText(MovieItemUtils.formatRating(data.getVoteAverage()));
		yearTextView.setText(MovieItemUtils.formatReleaseYear(data.getReleaseDate()));
		genreTextView.setText(MovieItemUtils.formatGenres(data.getGenreIds()));
		overviewTextView.setText(data.getOverview());
	}
}
