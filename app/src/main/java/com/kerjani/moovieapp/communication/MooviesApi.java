package com.kerjani.moovieapp.communication;

import android.os.Bundle;
import android.util.Log;

import com.kerjani.moovieapp.DetailsActivity;
import com.kerjani.moovieapp.util.MovieItemUtils;
import com.omertron.themoviedbapi.model.Genre;
import com.omertron.themoviedbapi.model.movie.MovieBasic;
import com.omertron.themoviedbapi.model.person.PersonFind;
import com.omertron.themoviedbapi.model.tv.TVBasic;
import com.omertron.themoviedbapi.results.WrapperGenericList;
import com.omertron.themoviedbapi.results.WrapperGenres;

import java.io.Serializable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 *
 */
public class MooviesApi {

	private static final String API_KEY = "0a08e38b874d0aa2d426ffc04357069d";
	private static final String DEFAULT_POPULARITY_ORDER = "popularity.desc";
	private static final String BASE_URL = "http://api.themoviedb.org/3/";
	public static Retrofit retrofit = new Retrofit.Builder()
			.baseUrl(BASE_URL)
			.addConverterFactory(JacksonConverterFactory.create())
			.build();

	private MooviesApi() {
		throw new IllegalStateException("Private constructor for utility class$");
	}

	protected static void loadPopularMovies(final Callback<WrapperGenericList<MovieBasic>> callBack) {
		final Call<WrapperGenericList<MovieBasic>> call = retrofit.create(TheMoviesDb.class)
				.getMovies(API_KEY, DEFAULT_POPULARITY_ORDER);
		call.enqueue(callBack);
	}

	public static void loadMovies(final Callback<WrapperGenericList<MovieBasic>> callBack) {
		final Call<WrapperGenres> call = retrofit.create(TheMoviesDb.class).getMovieGenres(API_KEY);
		call.enqueue(new Callback<WrapperGenres>() {
			@Override
			public void onResponse(final Call<WrapperGenres> call, final Response<WrapperGenres> response) {
				Log.d("GetMovieGenres", response == null ? "response is null" : response.toString());
				for (Genre genre : response.body().getGenres()) {
					MovieItemUtils.GENRES.put(genre.getId(), genre.getName());
				}
				loadPopularMovies(callBack);
			}

			@Override
			public void onFailure(final Call<WrapperGenres> call, final Throwable t) {
				Log.d("GetMovieGenres error", t.getMessage());
				callBack.onFailure(null, t);
			}
		});
	}

	protected static void loadPopularTVShows(final Callback<WrapperGenericList<TVBasic>> callBack) {
		final Call<WrapperGenericList<TVBasic>> call = retrofit.create(TheMoviesDb.class)
				.getTvShows(API_KEY, DEFAULT_POPULARITY_ORDER);
		call.enqueue(callBack);
	}

	public static void loadTvShows(final Callback<WrapperGenericList<TVBasic>> callBack) {
		final Call<WrapperGenres> call = retrofit.create(TheMoviesDb.class).getTvGenres(API_KEY);
		call.enqueue(new Callback<WrapperGenres>() {
			@Override
			public void onResponse(final Call<WrapperGenres> call, final Response<WrapperGenres> response) {
				Log.d("GetTvGenres", response == null ? "response is null" : response.toString());
				for (Genre genre : response.body().getGenres()) {
					MovieItemUtils.GENRES.put(genre.getId(), genre.getName());
				}
				loadPopularTVShows(callBack);
			}

			@Override
			public void onFailure(final Call<WrapperGenres> call, final Throwable t) {
				Log.d("GetTvGenres error", t.getMessage());
				callBack.onFailure(null, t);
			}
		});
	}

	protected static void loadPopularPersons(final Callback<WrapperGenericList<PersonFind>> callBack) {
		final Call<WrapperGenericList<PersonFind>> call = retrofit.create(TheMoviesDb.class)
				.getPersons(API_KEY, DEFAULT_POPULARITY_ORDER);
		call.enqueue(callBack);
	}

	public static void loadPersons(final Callback<WrapperGenericList<PersonFind>> callBack) {
		loadPopularPersons(callBack);
	}

	public static Bundle buildDetailsExtra(final String detailsFragmentClassName, final Serializable dataItem,
										   final String title, final String imageUrl) {
		Bundle detailsExtra = new Bundle();
		detailsExtra.putString(DetailsActivity.BUNDLE_KEY_DETAIL_FRAGMENT_CLASS_NAME, detailsFragmentClassName);
		detailsExtra.putSerializable(DetailsActivity.BUNDLE_KEY_ITEM, dataItem);
		detailsExtra.putString(DetailsActivity.BUNDLE_KEY_TITLE, title);
		detailsExtra.putString(DetailsActivity.BUNDLE_KEY_IMAGE_URL, imageUrl);
		return detailsExtra;
	}


	public interface TheMoviesDb {
		//		https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=0a08e38b874d0aa2d426ffc04357069d
		@GET("discover/movie")
		Call<WrapperGenericList<MovieBasic>> getMovies(@Query("api_key") String apiKey, @Query("sort_by") String sortBy);

		//		https://api.themoviedb.org/3/genre/movie/list?api_key=0a08e38b874d0aa2d426ffc04357069d
		@GET("genre/movie/list")
		Call<WrapperGenres> getMovieGenres(@Query("api_key") String owner);

		//		https://api.themoviedb.org/3/discover/tv?sort_by=popularity.desc&api_key=0a08e38b874d0aa2d426ffc04357069d
		@GET("discover/tv")
		Call<WrapperGenericList<TVBasic>> getTvShows(@Query("api_key") String apiKey, @Query("sort_by") String sortBy);

		//		https://api.themoviedb.org/3/genre/tv/list?api_key=0a08e38b874d0aa2d426ffc04357069d
		@GET("genre/tv/list")
		Call<WrapperGenres> getTvGenres(@Query("api_key") String owner);

		//		https://api.themoviedb.org/3/person/popular?sort_by=popularity.desc&api_key=0a08e38b874d0aa2d426ffc04357069d
		@GET("person/popular")
		Call<WrapperGenericList<PersonFind>> getPersons(@Query("api_key") String apiKey, @Query("sort_by") String sortBy);
	}

}
