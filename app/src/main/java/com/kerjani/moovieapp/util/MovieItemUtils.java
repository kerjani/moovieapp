package com.kerjani.moovieapp.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public final class MovieItemUtils {

	public static final String SIZE_SMALL = "w92";
	public static final String SIZE_DETAIL = "w342";
	public static final String BASE_IMAGE_URL = "http://image.tmdb.org/t/p/%1$s/%2$s";
	public static final String SEPARATOR = ", ";
	public static Map<Integer, String> GENRES = new HashMap<>();

	private MovieItemUtils() {
		throw new IllegalStateException("Private constructor for utility class.");
	}

	public static String getSmallImageUrl(@NonNull final String imagePath) {
		return String.format(BASE_IMAGE_URL, SIZE_SMALL, imagePath);
	}

	public static String getDetailmageUrl(@NonNull final String imagePath) {
		return String.format(BASE_IMAGE_URL, SIZE_DETAIL, imagePath);
	}

	@NonNull
	public static final String formatRating(final float rating) {
		return new DecimalFormat("#.#").format(rating);
	}

	@NonNull
	public static String formatGenres(@Nullable final List<Integer> genreIds) {
		if (genreIds == null || genreIds.size() == 0) {
			return "";
		}

		final String firstGenre = getGenreName(genreIds.get(0));
		final StringBuilder genresBuilder = new StringBuilder(firstGenre);
		final int genreListSize = genreIds.size();
		for (int i = 1; i < genreListSize; i++) {
			final String genreName = getGenreName(genreIds.get(i));
			if (genreName != null) {
				genresBuilder.append(SEPARATOR).append(genreName);
			}
		}

		return genresBuilder.toString();
	}

	@Nullable
	private static String getGenreName(final int id) {
		return GENRES.get(id);
	}

	@NonNull
	public static String formatReleaseYear(@NonNull final String fullDate) {
		try {
			final Date releaseDate = new SimpleDateFormat("yyyy-mm-dd").parse(fullDate);
			final Calendar calendar = Calendar.getInstance();
			calendar.setTime(releaseDate);
			return Integer.toString(calendar.get(Calendar.YEAR));
		} catch (ParseException e) {
			return "";
		}
	}
}
