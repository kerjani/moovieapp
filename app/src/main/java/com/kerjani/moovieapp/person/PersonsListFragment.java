package com.kerjani.moovieapp.person;

import android.util.Log;

import com.kerjani.moovieapp.BaseListFragment;
import com.kerjani.moovieapp.communication.MooviesApi;
import com.omertron.themoviedbapi.model.person.PersonFind;
import com.omertron.themoviedbapi.results.WrapperGenericList;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 */
public class PersonsListFragment extends BaseListFragment {
	@Override
	protected void loadData() {
		MooviesApi.loadPersons(new Callback<WrapperGenericList<PersonFind>>() {
			@Override
			public void onResponse(final Call<WrapperGenericList<PersonFind>> call,
								   final Response<WrapperGenericList<PersonFind>> response) {
				Log.w(getClass().getName(), response == null ? "response is null" : response.toString());
				final List<PersonFind> itemList = response.body().getResults();
				recyclerView.setAdapter(new PersonsRecyclerViewAdapter(itemList));
				if (itemList.size() > 0) {
					hideInfoViews();
				} else {
					showEmptyView();
				}
			}

			@Override
			public void onFailure(final Call<WrapperGenericList<PersonFind>> call, final Throwable t) {
				Log.w(getClass().getName(), t.getMessage());
				showRefreshView();
			}
		});
	}
}
