package com.kerjani.moovieapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 *
 */
public abstract class BaseListFragment extends Fragment {
	protected RecyclerView recyclerView;
	protected GridLayoutManager gridLayoutManager;
	protected View emptyView;
	protected View refreshView;

	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		gridLayoutManager = new GridLayoutManager(getContext(), getResources().getInteger(R.integer.movies_grid_column_count));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.fragment_movie_list, container, false);
		recyclerView = (RecyclerView) view.findViewById(R.id.list);
		emptyView = view.findViewById(R.id.empty_view);
		refreshView = view.findViewById(R.id.refresh_view);
		refreshView.setOnClickListener(view1 -> loadData());
		recyclerView.setLayoutManager(gridLayoutManager);
		return view;
	}

	@Override
	public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		loadData();
	}

	protected abstract void loadData();

	protected void hideInfoViews() {
		emptyView.setVisibility(View.GONE);
		refreshView.setVisibility(View.GONE);
	}

	protected void showEmptyView() {
		emptyView.setVisibility(View.VISIBLE);
		refreshView.setVisibility(View.GONE);
	}

	protected void showRefreshView() {
		emptyView.setVisibility(View.GONE);
		refreshView.setVisibility(View.VISIBLE);
	}
}
