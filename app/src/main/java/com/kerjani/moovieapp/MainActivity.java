package com.kerjani.moovieapp;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.kerjani.moovieapp.movie.MoviesFragment;
import com.kerjani.moovieapp.person.PersonsListFragment;
import com.kerjani.moovieapp.tv.TvShowsFragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

	private static final String STATE_CONTENT_FRAGMENT = "MainActivity:ContentFragment";
	private static final String DEFAULT_CONTENT_FRAGMENT_CLASS_NAME = MoviesFragment.class.getName();

	private String contentFragmentClassName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.addDrawerListener(toggle);
		toggle.syncState();

		final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);
		if (savedInstanceState != null) {
			contentFragmentClassName = savedInstanceState.getString(STATE_CONTENT_FRAGMENT);
		} else {
			contentFragmentClassName = DEFAULT_CONTENT_FRAGMENT_CLASS_NAME;
		}
		loadContentFragment();
	}

	private void loadContentFragment() {
		final Fragment contentFragment = Fragment.instantiate(this, contentFragmentClassName);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.content_main, contentFragment)
				.commit();
	}


	@Override
	protected void onSaveInstanceState(final Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(STATE_CONTENT_FRAGMENT, contentFragmentClassName);
	}

	@Override
	public void onBackPressed() {
		final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_settings:
				return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@SuppressWarnings("StatementWithEmptyBody")
	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		boolean needReload = false;
		switch (item.getItemId()) {
			case R.id.nav_movies:
				if (!contentFragmentClassName.equals(MoviesFragment.class.getName())) {
					contentFragmentClassName = MoviesFragment.class.getName();
					needReload = true;
				}
				break;
			case R.id.nav_tv_shows:
				if (!contentFragmentClassName.equals(TvShowsFragment.class.getName())) {
					contentFragmentClassName = TvShowsFragment.class.getName();
					needReload = true;
				}
				break;
			case R.id.nav_people:
				if (!contentFragmentClassName.equals(PersonsListFragment.class.getName())) {
					contentFragmentClassName = PersonsListFragment.class.getName();
					needReload = true;
				}
				break;
		}
		if (needReload) {
			loadContentFragment();
		}

		final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}
}
