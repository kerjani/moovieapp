package com.kerjani.moovieapp.movie;

import android.util.Log;

import com.kerjani.moovieapp.BaseListFragment;
import com.kerjani.moovieapp.communication.MooviesApi;
import com.omertron.themoviedbapi.model.movie.MovieBasic;
import com.omertron.themoviedbapi.results.WrapperGenericList;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 */
public class MoviesFragment extends BaseListFragment {

	protected void loadData() {
		MooviesApi.loadMovies(new Callback<WrapperGenericList<MovieBasic>>() {
			@Override
			public void onResponse(final Call<WrapperGenericList<MovieBasic>> call, final Response<WrapperGenericList<MovieBasic>> response) {
				Log.w(getClass().getName(), response == null ? "response is null" : response.toString());
				final List<MovieBasic> itemList = response.body().getResults();
				recyclerView.setAdapter(new MoviesRecyclerViewAdapter(itemList));
				if (itemList.size() > 0) {
					hideInfoViews();
				} else {
					showEmptyView();
				}
			}

			@Override
			public void onFailure(final Call<WrapperGenericList<MovieBasic>> call, final Throwable t) {
				Log.w(getClass().getName(), t.getMessage());
				showRefreshView();
			}
		});
	}

}
