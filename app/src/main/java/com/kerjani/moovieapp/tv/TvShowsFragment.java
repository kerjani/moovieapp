package com.kerjani.moovieapp.tv;

import android.util.Log;

import com.kerjani.moovieapp.BaseListFragment;
import com.kerjani.moovieapp.communication.MooviesApi;
import com.omertron.themoviedbapi.model.tv.TVBasic;
import com.omertron.themoviedbapi.results.WrapperGenericList;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 */
public class TvShowsFragment extends BaseListFragment {
	@Override
	protected void loadData() {
		MooviesApi.loadTvShows(new Callback<WrapperGenericList<TVBasic>>() {
			@Override
			public void onResponse(final Call<WrapperGenericList<TVBasic>> call, final Response<WrapperGenericList<TVBasic>> response) {
				Log.w(getClass().getName(), response == null ? "response is null" : response.toString());
				final List<TVBasic> itemList = response.body().getResults();
				recyclerView.setAdapter(new TvShowsRecyclerViewAdapter(itemList));
				if (itemList.size() > 0) {
					hideInfoViews();
				} else {
					showEmptyView();
				}
			}

			@Override
			public void onFailure(final Call<WrapperGenericList<TVBasic>> call, final Throwable t) {
				Log.w(getClass().getName(), t.getMessage());
				showRefreshView();
			}
		});
	}
}
